/*************************bài 1***************/
document.getElementById("btnTimSND").onclick = function () {
  var tong = 0;
  var soLonNhat = 0;
  for (var n = 1; n < 10000; n++) {
    tong += n;
    if (tong < 10000) {
      soLonNhat = n;
    }
  }
  document.getElementById("showBai1").innerHTML = soLonNhat;
};
/*************************bài 2***************/
document.getElementById("btnTinhTong").onclick = function () {
  //  input: number
  var numberX = document.getElementById("txt-inputX").value * 1;
  var numberN = document.getElementById("txt-inputN").value * 1;
  // output: number
  var tong = 0;
  //xử lý
  for (var i = 1; i <= numberN; i++) {
    tong += Math.pow(numberX, i);
  }
  document.getElementById("showBai2").innerText = tong;
};

/*************************bài 3***************/
document.getElementById("btnTinhGiaiThua").onclick = function () {
  //input: number
  var inputBai3 = document.getElementById("txt-number").value * 1;
  // output: string
  var giaiThua = 1;
  //xử lý
  for (soHang = 1; soHang <= inputBai3; soHang++) {
    giaiThua *= soHang;
  }
  document.getElementById("showBai3").innerText = giaiThua;
};

/*************************bài 4***************/
document.getElementById("btnTaoDiv").onclick = function () {
  // input: number
  var inputBai4 = document.getElementById("numberDiv").value * 1;
  // output: string
  var tagDiv = "";
  // xử lý
  for (var count = 1; count <= inputBai4; count++) {
    if (count % 2 === 0) {
      tagDiv += `<p class= "bg-danger text-white pl-3">Div chẵn ${count}</p>`;
    } else {
      tagDiv += `<p class= "bg-info text-white pl-3">Div lẽ ${count}</p>`;
    }
  }
  document.getElementById("showBai4").innerHTML = tagDiv;
};

/*************************bài 5***************/
// hàm check nguyên tố
function kiemTraSNT(number) {
  var checkSNT = true;
  for (var i = 2; i <= Math.sqrt(number); i++) {
    if (number % i === 0) {
      checkSNT = false;
    }
  }
  return checkSNT;
}
document.getElementById("btnTinhBai5").onclick = function () {
  // input: number
  var inputBai5 = document.getElementById("numberBai5").value * 1;
  // output: string;
  var ketQua = "";
  // xử lý
  for (var number = 2; number < inputBai5; number++) {
    var checkSNT = kiemTraSNT(number);
    if (checkSNT) {
      ketQua += number + " ";
    }
  }
  document.getElementById("showBai5").innerText = ketQua;
};
